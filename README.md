# GameDash

### install

- clone git repo
- install deps `yarn`
- start dev server `yarn start`

### build

- build `yarn build`
- build & deploy `yarn deploy`

### assets

> batch converters

- https://online-audio-converter.com/
- https://imagecompressor.com/ (bitmap)
- https://vecta.io/nano (svg batch)
