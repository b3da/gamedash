import React from 'react'
import './App.css'
import { Router, StoreContextProvider } from './lib'

function App() {
  return (
    <div className="App">
      <StoreContextProvider>
        <Router />
      </StoreContextProvider>
    </div>
  )
}

export default App
