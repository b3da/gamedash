import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import * as serviceWorkerRegistration from './serviceWorkerRegistration'
import reportWebVitals from './reportWebVitals'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.unregister()

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()

// todo dependency hotfix for cleaner dev console
;(() => {
  const oldLogError = console.error
  console.error = function (...args) {
    if (
      typeof args[0] !== 'string' ||
      !(
        args[0].includes('is deprecated in StrictMode') &&
        args[1].includes('findDOMNode') &&
        args[3].includes('Transition')
      )
    ) {
      oldLogError.apply(console, args)
    }
  }
})()
