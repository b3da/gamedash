import React, { useEffect, useState } from 'react'
import { NavLink, useHistory } from 'react-router-dom'
import { Rules as RulesType, useStore } from '../lib'

export const Rules = () => {
  const [rules, setRules] = useState<Array<{ id: number; cluster: string; rule: string }>>([])
  const history = useHistory()
  const { state, patchState } = useStore()

  useEffect(() => {
    const r = Object.keys(state.rules).map(id => ({
      id: Number(id),
      cluster: state.rules[Number(id)].cluster,
      rule: state.rules[Number(id)].rule,
    }))
    setRules(r.sort((a, b) => (a.cluster === b.cluster ? 0 : a.cluster > b.cluster ? 1 : -1)))
  }, [])

  const remove = (id: number) => {
    if (!confirm('confirm delete')) {
      return
    }
    const nextRules: RulesType = { ...state.rules }
    delete nextRules[id]
    patchState({ rules: nextRules })
    setTimeout(() => history.push('/rules'), 100)
  }

  return (
    <div className="view">
      <div className="self-start w-full">
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                {/*<th>id</th>*/}
                <th>cluster</th>
                <th>rule</th>
                <th style={{ padding: '3px' }}>
                  <NavLink to={`/rule`} className="btn btn-primary mr-2" style={{ float: 'right' }}>
                    &nbsp;create new rule&nbsp;
                  </NavLink>
                </th>
              </tr>
            </thead>
            <tbody>
              {rules.map(rule => (
                <tr key={`rule-${rule.id}`} className="hover">
                  {/*<th>{rule.id}</th>*/}
                  <th>{rule.cluster}</th>
                  <td>{rule.rule.length > 60 ? `${rule.rule.substr(0, 60)}...` : rule.rule}</td>
                  <td style={{ padding: '3px', textAlign: 'right', width: '150px' }}>
                    <div style={{ maxWidth: '150px' }}>
                      <NavLink to={`/rule/${rule.id}`} className="btn mr-2">
                        edit
                      </NavLink>
                      <button className="btn btn-error" onClick={() => remove(rule.id)}>
                        delete
                      </button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}
