import React from 'react'
import { NavLink, useHistory } from 'react-router-dom'
import { GameServer, useStore } from '../lib'

export const Servers = () => {
  const history = useHistory()
  const { state, patchState } = useStore()

  const remove = (uuid: string) => {
    if (!confirm('confirm delete')) {
      return
    }
    const nextServers: GameServer[] = state.servers.filter(s => s.id !== uuid)
    patchState({ servers: nextServers })
    setTimeout(() => history.push('/servers'), 100)
  }

  return (
    <div className="view">
      <div className="self-start w-full">
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                <th>name</th>
                <th>cluster</th>
                <th>ip</th>
                <th>game port</th>
                <th>RCON port</th>
                <th style={{ padding: '3px' }}>
                  <NavLink to={`/server`} className="btn btn-primary mr-2" style={{ float: 'right' }}>
                    create new server
                  </NavLink>
                </th>
              </tr>
            </thead>
            <tbody>
              {state.servers.map(server => (
                <tr key={server.id} className="hover">
                  <th>{server.name}</th>
                  <td>{server.cluster}</td>
                  <td>{server.ip}</td>
                  <td>{server.portGame}</td>
                  <td>{server.portRCON}</td>
                  <td style={{ padding: '3px', textAlign: 'right', width: '160px' }}>
                    <div style={{ maxWidth: '160px' }}>
                      <NavLink to={`/server/${server.id}`} className="btn mr-2">
                        edit
                      </NavLink>
                      <button className="btn btn-error" onClick={() => remove(server.id)}>
                        delete
                      </button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}
