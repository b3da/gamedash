import React, { SyntheticEvent, useEffect, useState } from 'react'
import { NavLink, useHistory, useParams } from 'react-router-dom'
import {
  Record as RecordType,
  RecordSet as RecordSetType,
  useStore,
} from '../lib'

export const Record = () => {
  const [returnTo, setReturnTo] = useState<string>('/records')
  const [isCreating, setIsCreating] = useState<boolean>(true)
  const [isAutocompleteOpen, setIsAutocompleteOpen] = useState<boolean>(false)
  const [recordId, setRecordId] = useState<number>(0)
  const [recordSet, setRecordSet] = useState<RecordSetType>()
  const [balance, setBalance] = useState<number>(0)
  const [note, setNote] = useState<string>('')
  const [user, setUser] = useState<string>('')
  const [users, setUsers] = useState<string[]>([])
  const [userSuggestions, setUserSuggestions] = useState<string[]>([])
  const [recordData, setRecordData] = useState<any>({})
  const history = useHistory()
  const { id, username }: any = useParams()
  const { state, patchState } = useStore()
  const allSets: RecordSetType[] = ['arena', 'donator', 'event', 'withdrawal']

  const fillData = (record: RecordType) => {
    setIsCreating(false)
    setRecordId(id)
    setBalance(record.data.balance)
    setNote(record.data.note)
    setUser(record.data.user)
    setRecordSet(record.set)
    setRecordData(record.data)
  }

  useEffect(() => {
    if (id && !isNaN(Number(id)) && Number(id) >= 0) {
      const record = state.records.filter(r => r.id === Number(id))[0]
      if (record) {
        fillData(record)
      } else {
        setRecordId(state.records.length)
      }
    } else {
      setRecordId(state.records.length)
    }
    setUsers(Array.from(new Set(state.records.map(r => r.data.user))))
    if (username?.length > 0) {
      setUser(username)
      setReturnTo(`/user/${username}`)
    }
  }, [id])

  const searchForUser = (val: string) => {
    if (!users.includes(val)) {
      setUserSuggestions(
        users.filter(u =>
          u
            .toLowerCase()
            .normalize('NFD')
            .replace(/[\u0300-\u036f]/g, '')
            .includes(val.toLowerCase()),
        ),
      )
      setIsAutocompleteOpen(true)
    } else {
      setIsAutocompleteOpen(false)
    }
    setUser(val)
  }

  const create = () => {
    const record: RecordType = {
      id: recordId,
      set: recordSet!,
      data: {
        createdBy: state.username,
        createdOn: +new Date(),
        balance,
        note,
        user,
      },
    }
    const nextRecords: RecordType[] = [...state.records, record]
    patchState({ records: nextRecords })
    setTimeout(() => history.push(returnTo), 100)
  }

  const update = () => {
    // no update for records
  }

  const remove = () => {
    // no remove for records
  }

  const onSubmit = (event: SyntheticEvent) => {
    event.preventDefault()
    if (!recordSet) {
      return
    }
    if (isCreating) {
      create()
    } else {
      update()
    }
  }

  return (
    <div className="view">
      <div className="self-start w-full p-10">
        <form onSubmit={onSubmit} className="w-full flex flex-col md:flex-row">
          <div className="w-full md:w-1/2">
            {/*<div className="form-control mb-2">*/}
            {/*  <label className="label">*/}
            {/*    <span className="label-text">unique id:</span>*/}
            {/*  </label>*/}
            {/*  <input type="text" className="input input-bordered" value={recordId} readOnly={true} />*/}
            {/*</div>*/}
            <div className="form-control mb-2 relative">
              <label className="label">
                <span className="label-text">user:</span>
              </label>
              <input
                type="text"
                className="input input-bordered"
                readOnly={!isCreating}
                value={user}
                onChange={e => searchForUser(e.target.value)}
                onBlur={() => setTimeout(() => setIsAutocompleteOpen(false), 200)}
              />
              {isAutocompleteOpen && userSuggestions.length > 0 && (
                <div className="autocomplete-container">
                  {userSuggestions.map(us => (
                    <div key={`autocomplete-${us}`} onClick={() => searchForUser(us)}>
                      {us}
                    </div>
                  ))}
                </div>
              )}
            </div>
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">note:</span>
              </label>
              <input
                type="text"
                className="input input-bordered"
                readOnly={!isCreating}
                value={note}
                onChange={e => setNote(e.target.value)}
              />
            </div>
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">balance change:</span>
              </label>
              <input
                type="number"
                className="input input-bordered"
                readOnly={!isCreating}
                value={`${balance}`}
                onChange={e => setBalance(Number(e.target.value))}
              />
            </div>
          </div>
          <div className="w-full md:w-1/2 md:pl-5">
            {!!recordData?.createdBy && (
              <div>
                <div className="form-control mb-2">
                  <label className="label">
                    <span className="label-text">created by:</span>
                  </label>
                  <input type="text" className="input input-bordered" value={recordData.createdBy} readOnly={true} />
                </div>
                <div className="form-control mb-2">
                  <label className="label">
                    <span className="label-text">created on:</span>
                  </label>
                  <input
                    type="text"
                    className="input input-bordered"
                    value={new Date(recordData.createdOn).toISOString()}
                    readOnly={true}
                  />
                </div>
              </div>
            )}
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">set:</span>
              </label>
              <div className="flex mt-2">
                {allSets.map(s => (
                  <div
                    key={`set-${s}`}
                    className={`badge badge-lg cursor-pointer mr-2 ${
                      recordSet === s ? 'badge-primary' : 'badge-ghost'
                    }`}
                    onClick={() => (isCreating ? setRecordSet(s) : null)}
                  >
                    {s}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </form>
        <div className="flex flex-row mt-2 justify-end">
          <div>
            <NavLink to={returnTo} className="btn">
              back
            </NavLink>
          </div>
          {isCreating && (
            <button className="btn btn-primary ml-2" onClick={onSubmit} disabled={user?.length < 3 || !recordSet}>
              create
            </button>
          )}
        </div>
      </div>
    </div>
  )
}
