import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { Record, useStore } from '../lib'

export const Records = () => {
  const [records, setRecords] = useState<Record[]>([])
  const { state } = useStore()

  useEffect(() => {
    setRecords([...state.records.sort((a, b) => (a.id === b.id ? 0 : a.id < b.id ? 1 : -1))]) // todo searchable
  }, [])

  return (
    <div className="view">
      <div className="self-start w-full">
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                <th>id</th>
                <th>user</th>
                <th>set</th>
                <th>balance</th>
                <th>note</th>
                <th style={{ padding: '3px' }}>
                  <NavLink to={`/record`} className="btn btn-primary mr-2" style={{ float: 'right' }}>
                    &nbsp;create new record&nbsp;
                  </NavLink>
                </th>
              </tr>
            </thead>
            <tbody>
              {records.map(record => (
                <tr key={`record-${record.id}`} className="hover">
                  <th>{record.id}</th>
                  <th>
                    <NavLink to={`/user/${record.data.user}`}>{record.data.user}</NavLink>
                  </th>
                  <th>{record.set}</th>
                  <th>{record.data.balance}</th>
                  <th>{record.data.note}</th>
                  <td style={{ padding: '3px', textAlign: 'right' }}>
                    <div>
                      <NavLink to={`/record/${record.id}`} className="btn mr-2" style={{ float: 'right' }}>
                        detail
                      </NavLink>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}
