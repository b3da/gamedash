import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { extractDataForAllUsers, GameServer, Rules as RulesType, useStore } from '../lib'

export const JSONViewer = () => {
  const [rules, setRules] = useState<Array<{ id: number; cluster: string; rule: string }>>([])
  const [servers, setServers] = useState<GameServer[]>([])
  const [response, setResponse] = useState<any>()
  const { state } = useStore()
  const { resource }: any = useParams()

  useEffect(() => {
    const r = Object.keys(state.rules).map(id => ({
      id: Number(id),
      cluster: state.rules[Number(id)].cluster,
      rule: state.rules[Number(id)].rule,
    }))
    const rs = r.sort((a, b) => (a.cluster === b.cluster ? 0 : a.cluster > b.cluster ? 1 : -1))
    setRules(rs)
    setServers([...state.servers])
    let clusters: string[] = []
    const res: any = []
    switch (resource) {
      case 'servers':
        clusters = Array.from(new Set(state.servers.map(s => s.cluster)))
        for (const c of clusters) {
          res.push({
            cluster: c,
            servers: state.servers
              .filter(s => s.cluster === c)
              .map(s => ({
                name: s.name,
                description: s.description,
                ip: s.ip,
                port: s.portQuery,
                RCON: s.portRCON,
                steamLink: `steam://connect/${s.ip}:${s.portQuery}`,
                isAlive: 'todo', // todo
              })),
          })
        }
        setResponse(res)
        break
      case 'rules':
        clusters = Array.from(new Set(rs.map(a => a.cluster)))
        for (const c of clusters) {
          res.push({
            cluster: c,
            rules: rs.filter(a => a.cluster === c).map(a => a.rule),
          })
        }
        setResponse(res)
        break
      case 'users':
        setResponse(extractDataForAllUsers(state))
        break
    }
  }, [])

  if (!response) {
    return null
  }

  return (
    <div className="view">
      <div className="view-content">
        <pre style={{ textAlign: 'left' }}>{JSON.stringify(response, null, 2)}</pre>
      </div>
    </div>
  )
}
