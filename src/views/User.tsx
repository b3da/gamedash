import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { extractDataForUser, Record, useStore } from '../lib'
import { Countdown } from '../components'

export const User = () => {
  const [user, setUser] = useState<any>()
  const [totalPointsAcquired, setTotalPointsAcquired] = useState<number>(0)
  const [totalPointsUsed, setTotalPointsUsed] = useState<number>(0)
  const [eventPointsAcquired, setEventPointsAcquired] = useState<number>(0)
  const [eventsAttended, setEventsAttended] = useState<number>(0)
  const [arenaPointsAcquired, setArenaPointsAcquired] = useState<number>(0)
  const [arenaTiersAccomplished, setArenaTiersAccomplished] = useState<number>(0)
  const [records, setRecords] = useState<Record[]>([])
  const { username }: any = useParams()
  const { state } = useStore()

  useEffect(() => {
    setUser(extractDataForUser(username, state))
    const nextRecords = [
      ...state.records.filter(r => r.data.user === username).sort((a, b) => (a.id === b.id ? 0 : a.id < b.id ? 1 : -1)),
    ]
    setRecords(nextRecords)
    let nextTotalPointsAcquired = 0
    try {
      nextTotalPointsAcquired = nextRecords
        .filter(r => (r.set === 'event' || r.set === 'arena') && r.data.balance > 0)
        .map(r => Number(r.data.balance))
        .reduce((a, b) => a + b)
    } catch (_) {}
    setTotalPointsAcquired(nextTotalPointsAcquired)
    let nextEventPointsAcquired = 0
    try {
      nextEventPointsAcquired = nextRecords
        .filter(r => r.set === 'event' && r.data.balance > 0)
        .map(r => Number(r.data.balance))
        .reduce((a, b) => a + b)
    } catch (_) {}
    setEventPointsAcquired(nextEventPointsAcquired)
    setEventsAttended(nextRecords.filter(r => r.set === 'event' && r.data.balance > 0).length)
    let nextArenaPointsAcquired = 0
    try {
      nextArenaPointsAcquired = nextRecords
        .filter(r => r.set === 'arena' && r.data.balance > 0)
        .map(r => Number(r.data.balance))
        .reduce((a, b) => a + b)
    } catch (_) {}
    setArenaPointsAcquired(nextArenaPointsAcquired)
    setArenaTiersAccomplished(nextRecords.filter(r => r.set === 'arena' && r.data.balance > 0).length)
    let nextTotalPointsUsed = 0
    try {
      nextTotalPointsUsed = nextRecords
        .filter(r => (r.set === 'event' || r.set === 'arena' || r.set === 'withdrawal') && r.data.balance < 0)
        .map(r => Number(r.data.balance))
        .reduce((a, b) => a + b)
    } catch (_) {}
    setTotalPointsUsed(nextTotalPointsUsed)
  }, [])

  if (!user) {
    return null
  }
  return (
    <div className="view">
      <div className="self-start w-full">
        <div className="w-full shadow stats">
          <div className="stat">
            <div className="stat-title">user</div>
            <div className="stat-value text-primary">{username}</div>
            <div className="stat-desc mt-1">{records.length} records</div>
          </div>
          <div className="stat">
            <div className="stat-figure text-info">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                className="inline-block w-8 h-8 stroke-current"
              >
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M13 10V3L4 14h7v7l9-11h-7z" />
              </svg>
            </div>
            <div className="stat-title">event points</div>
            <div className="stat-value text-info">{user.balanceEventPoints} EB</div>
            <div className="stat-desc mt-1">
              from {records.filter(r => r.set !== 'donator' && r.set !== 'withdrawal').length} events
            </div>
          </div>
          <div className="stat">
            {user.isDonator && (
              <div className="stat-figure text-primary">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  className="inline-block w-8 h-8 stroke-current"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
                  />
                </svg>
              </div>
            )}
            <div className="stat-title">{user.isDonator ? `donations [${user.donatedSum} CZK]` : ''}</div>
            <div className={`stat-value ${user.isDonator ? 'text-primary' : 'text-neutral'}`}>
              {user.isDonator ? (user.hasActiveMembership ? 'Member' : 'Donator') : '-'}
            </div>
            <div className="stat-desc mt-1">
              {user.hasActiveMembership ? `active till ${user.membershipEnd?.substr(0, 10)}` : ''}
            </div>
          </div>
        </div>
        <div className="text-left m-4">
          <ul>
            <li>Total event points acquired: {totalPointsAcquired} EB</li>
            <li>Total event points used: {totalPointsUsed} EB</li>
            <li>---</li>
            <li>Events attended: {eventsAttended}</li>
            <li>Total event points from events: {eventPointsAcquired} EB</li>
            <li>---</li>
            <li>Arena tiers accomplished: {arenaTiersAccomplished}</li>
            <li>Total event points from arena: {arenaPointsAcquired} EB</li>
          </ul>
          {user.isDonator && (
            <ul>
              <li>---</li>
              {/*<li>Total donations {user.donatedSum}</li>*/}
              {user.hasActiveMembership && (
                <li>
                  Membership ends in <Countdown targetTime={+new Date(user.membershipEnd)} />
                </li>
              )}
            </ul>
          )}
        </div>
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                {/*<th>id</th>*/}
                <th>created</th>
                <th>set</th>
                <th>balance</th>
                <th>note</th>
                <th style={{ padding: '3px' }}>
                  <NavLink to={`/record/-1/${username}`} className="btn btn-primary mr-2" style={{ float: 'right' }}>
                    &nbsp;create new record&nbsp;
                  </NavLink>
                </th>
              </tr>
            </thead>
            <tbody>
              {records.map(record => (
                <tr key={`record-${record.id}`} className="hover">
                  {/*<th>{record.id}</th>*/}
                  <th>{new Date(record.data.createdOn).toISOString().substr(0, 10)}</th>
                  <th>{record.set}</th>
                  <th>{record.data.balance}</th>
                  <th>{record.data.note}</th>
                  <td style={{ padding: '3px', textAlign: 'right' }}>
                    <div>
                      <NavLink to={`/record/${record.id}`} className="btn mr-2" style={{ float: 'right' }}>
                        detail
                      </NavLink>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}
