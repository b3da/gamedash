import React, { SyntheticEvent, useEffect, useState } from 'react'
import { NavLink, useHistory, useParams } from 'react-router-dom'
import { Rule as RuleType, useStore } from '../lib'

export const Rule = () => {
  const [isCreating, setIsCreating] = useState<boolean>(true)
  const [ruleId, setRuleId] = useState<number>(0)
  const [cluster, setCluster] = useState<string>('')
  const [rule, setRule] = useState<string>('')
  const history = useHistory()
  const { id }: any = useParams()
  const { state, patchState } = useStore()

  const fillData = (rule: RuleType) => {
    setIsCreating(false)
    setRuleId(Number(id))
    setRule(rule.rule)
    setCluster(rule.cluster)
  }

  useEffect(() => {
    console.warn('init', { id, ruleId }) // TODO: remove
    if (id && !isNaN(Number(id)) && id >= 0) {
      const rule = state.rules[id]
      if (rule) {
        fillData(rule)
      }
    }
  }, [id])

  useEffect(() => {
    console.warn('state up', { id, ruleId, state }) // TODO: remove
    if (id && !isNaN(Number(id)) && id >= 0) {
      const rule = state.rules[id]
      if (rule) {
        fillData(rule)
      }
    }
  }, [state])

  const create = () => {
    const newRule: RuleType = { cluster, rule }
    const nextRules = { ...state.rules }
    nextRules[Object.keys(state.rules).length] = newRule
    patchState({ rules: nextRules })
    setTimeout(() => history.push('/rules'), 100)
  }

  const update = () => {
    const nextRules = { ...state.rules }
    nextRules[ruleId] = { cluster, rule }
    patchState({ rules: nextRules })
    setTimeout(() => history.push('/rules'), 100)
  }

  const remove = () => {
    if (!confirm('confirm delete')) {
      return
    }
    const nextRules = { ...state.rules }
    delete nextRules[ruleId]
    patchState({ rules: nextRules })
    setTimeout(() => history.push('/rules'), 100)
  }

  const onSubmit = (event: SyntheticEvent) => {
    event.preventDefault()
    if (isCreating) {
      create()
    } else {
      update()
    }
  }

  return (
    <div className="view">
      <div className="self-start w-full p-10">
        <form onSubmit={onSubmit} className="w-full flex flex-col md:flex-row">
          <div className="w-full md:w-1/2">
            {/*<div className="form-control mb-2">*/}
            {/*  <label className="label">*/}
            {/*    <span className="label-text">unique id:</span>*/}
            {/*  </label>*/}
            {/*  <input type="text" className="input input-bordered" value={ruleId} onChange={e => setRuleId(Number(e.target.value))} />*/}
            {/*</div>*/}
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">cluster:</span>
              </label>
              <input
                type="text"
                className="input input-bordered"
                value={cluster}
                onChange={e => setCluster(e.target.value)}
              />
            </div>
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">rule:</span>
              </label>
              <input
                type="text"
                className="input input-bordered"
                value={rule}
                onChange={e => setRule(e.target.value)}
              />
            </div>
          </div>
        </form>
        <div className="flex flex-row mt-2 justify-end">
          <div>
            <NavLink to={'/rules'} className="btn">
              back
            </NavLink>
          </div>
          {!isCreating && (
            <button className="btn btn-error ml-2" onClick={remove}>
              delete
            </button>
          )}
          <button className="btn btn-primary ml-2" onClick={onSubmit} disabled={rule?.length < 3}>
            {isCreating ? 'create' : 'update'}
          </button>
        </div>
      </div>
    </div>
  )
}
