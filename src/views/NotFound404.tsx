import React from 'react'

export const NotFound404: React.FC = () => {
  return <div className="flex-1 p-10 text-2xl font-bold">404; nope</div>
}
