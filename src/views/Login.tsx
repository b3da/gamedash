import React, { SyntheticEvent, useState } from 'react'
import { NavLink, useHistory } from 'react-router-dom'
import { Storage, useStore } from '../lib'

export const Login = () => {
  const [username, setUsername] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const history = useHistory()
  const { state, setState } = useStore()
  const login = (event: SyntheticEvent) => {
    if (event) {
      event.preventDefault()
    }
    if (state.username !== username) {
      Storage.remove()
      setState({
        username,
        records: [], // todo fetch from gitlab repo json
        rules: {},
        servers: [],
      })
    }
    history.push('/dashboard')
  }
  return (
    <div className="view">
      <form
        onSubmit={login}
        className="p-10"
        style={{
          maxWidth: window.innerWidth > 768 ? '35vw' : '96vw',
          width: window.innerWidth > 768 ? '35vw' : '96vw',
        }}
      >
        <h1 className="text-left text-2xl font-bold mb-5">login</h1>
        <div className="form-control mb-2">
          <input
            type="text"
            className="input input-primary input-lg"
            placeholder="username"
            value={username}
            onChange={e => setUsername(e.target.value)}
          />
        </div>
        <div className="form-control mb-5">
          <input
            type="password"
            className="input input-primary input-lg"
            placeholder="password"
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
        </div>
        <button className="btn btn-primary btn-block mb-5 text-2xl" onClick={login} disabled={username?.length < 3}>
          sign in
        </button>
        <div>
          <NavLink to={'/'} className="btn btn-block btn-link">
            back to root
          </NavLink>
        </div>
      </form>
    </div>
  )
}
