import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { Storage } from '../lib'

export const PublicHome: React.FC = () => {
  // const [users, setUsers] = useState<any[]>([])

  // useEffect(() => {
  //
  // }, [])

  return (
    <div className="view">
      <div
        className="p-10"
        style={{
          maxWidth: window.innerWidth > 768 ? '70vw' : '96vw',
          width: window.innerWidth > 768 ? '70vw' : '96vw',
        }}
      >
        <h1 className="text-left text-2xl font-bold mb-5">public root</h1>
        <p className="text-left">
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque sapien. Etiam posuere lacus quis dolor.
          Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus.
          Mauris elementum mauris vitae tortor. Morbi leo mi, nonummy eget tristique non, rhoncus non leo.
        </p>
        <div className="mt-5">
          <NavLink to={'/login'} className="text-primary text-2xl btn">
            Login
          </NavLink>
        </div>
      </div>
    </div>
  )
}
