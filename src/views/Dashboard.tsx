import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import { Countdown } from '../components'
import { extractDataForAllUsers, useStore } from '../lib'

export const Dashboard = () => {
  const [searchPhrase, setSearchPhrase] = useState<string>('')
  const [allUsers, setAllUsers] = useState<any[]>([])
  const [users, setUsers] = useState<any[]>([])
  const { state } = useStore()

  useEffect(() => {
    const u = extractDataForAllUsers(state)
    setAllUsers(u)
    setUsers(sortUsers(u))
  }, [state])

  const search = (phrase: string) => {
    setSearchPhrase(phrase)
    setUsers(
      sortUsers(
        allUsers.filter(u =>
          u.user
            .toLowerCase()
            .normalize('NFD')
            .replace(/[\u0300-\u036f]/g, '')
            .includes(phrase.toLowerCase()),
        ),
      ),
    )
  }

  const sortUsers = (u: any[]) => {
    return u.sort((a, b) => (a.user === b.user ? 0 : a.user > b.user ? 1 : -1))
  }

  return (
    <div className="view">
      <div className="view-content">
        <div className="w-full flex flex-row">
          <div className="pl-3 pr-1 w-1/2">
            <div>
              <NavLink to={'/servers'} className="text-2xl btn btn-block mb-2">
                servers
              </NavLink>
            </div>
            <div>
              <NavLink to={'/rules'} className="text-2xl btn btn-block mb-2">
                rules
              </NavLink>
            </div>
            <div>
              <NavLink to={'/records'} className="text-2xl btn btn-block mb-2">
                records
              </NavLink>
            </div>
          </div>
          <div className="pl-1 pr-3 w-1/2">
            <div>
              <NavLink to={'/json/servers'} className="text-2xl btn btn-block mb-2">
                servers JSON
              </NavLink>
            </div>
            <div>
              <NavLink to={'/json/rules'} className="text-2xl btn btn-block mb-2">
                rules JSON
              </NavLink>
            </div>
            <div>
              <NavLink to={'/json/users'} className="text-2xl btn btn-block mb-2">
                users JSON
              </NavLink>
            </div>
          </div>
        </div>
        {allUsers?.length > 0 && (
          <div className="w-full mt-1">
            <div className="overflow-x-auto">
              <table className="table w-full">
                <thead>
                  <tr>
                    <th>user</th>
                    <th>arena tiers</th>
                    <th>events attended</th>
                    <th>balance</th>
                    <th>donator</th>
                    <th>member</th>
                    <th>membership end</th>
                    <th style={{ padding: '3px' }}>
                      <input
                        type="text"
                        className="input input-ghost mr-2"
                        style={{ float: 'right' }}
                        placeholder="Search"
                        value={searchPhrase}
                        onChange={e => search(e.target.value)}
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {users.map(user => (
                    <tr key={`user-${user.user}`} className="hover">
                      <th>
                        <NavLink to={`/user/${user.user}`}>{user.user}</NavLink>
                      </th>
                      <th>{user.records.filter((r: any) => r.set === 'arena')[0]?.data?.length || '-'}</th>
                      <th>{user.records.filter((r: any) => r.set === 'event')[0]?.data?.length || '-'}</th>
                      <th>{user.balanceEventPoints} EB</th>
                      <th>
                        {user.isDonator ? (
                          <div
                            key={`isdonator-${user.user}`}
                            className={`badge badge-lg cursor-pointer mr-2 badge-primary`}
                          >
                            donator
                          </div>
                        ) : null}
                      </th>
                      <th>
                        {user.hasActiveMembership ? (
                          <div
                            key={`ismember-${user.user}`}
                            className={`badge badge-lg cursor-pointer mr-2 badge-primary`}
                          >
                            member
                          </div>
                        ) : null}
                      </th>
                      {/*<td>{user.membershipDays > 0 ? `${formatCountdown(+new Date(user.membershipEnd) - +new Date())}` : ''}</td>*/}
                      <td>{user.membershipDays > 0 ? <Countdown targetTime={+new Date(user.membershipEnd)} /> : ''}</td>
                      <td style={{ padding: '3px', textAlign: 'right' }}>
                        <div>
                          <NavLink to={`/user/${user.user}`} className="btn mr-2" style={{ float: 'right' }}>
                            detail
                          </NavLink>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        )}
      </div>
    </div>
  )
}
