import React, { SyntheticEvent, useEffect, useState } from 'react'
import { NavLink, useHistory, useParams } from 'react-router-dom'
import { GameServer, GamingPlatform, generateUuidV4, useStore } from '../lib'

export const Server = () => {
  const [isCreating, setIsCreating] = useState<boolean>(true)
  const [uuid, setUuid] = useState<string>(generateUuidV4())
  const [name, setName] = useState<string>('')
  const [cluster, setCluster] = useState<string>('')
  const [description, setDescription] = useState<string>('')
  const [ip, setIp] = useState<string>('')
  const [portGame, setPortGame] = useState<number>(7777)
  const [portQuery, setPortQuery] = useState<number>(27015)
  const [portRCON, setPortRCON] = useState<number>(27020)
  const [platforms, setPlatforms] = useState<GamingPlatform[]>(['steam'])
  const history = useHistory()
  const { id }: any = useParams()
  const { state, patchState } = useStore()
  const allPlatforms: GamingPlatform[] = ['steam', 'epic', 'ps', 'xbox']

  const fillData = (server: GameServer) => {
    setIsCreating(false)
    setUuid(id)
    setName(server.name)
    setCluster(server.cluster)
    setDescription(server.description)
    setIp(server.ip)
    setPortGame(server.portGame)
    setPortQuery(server.portQuery)
    setPortRCON(server.portRCON)
    setPlatforms(server.platforms)
  }

  useEffect(() => {
    // console.warn('init', { id, uuid }) // TODO: remove
    if (id && id.length > 0) {
      const server = state.servers.filter(s => s.id === id)[0]
      if (server) {
        fillData(server)
      }
    }
  }, [id])

  useEffect(() => {
    // console.warn('state up', { id, uuid, state }) // TODO: remove
    if (id && id.length > 0) {
      const server = state.servers.filter(s => s.id === id)[0]
      if (server) {
        fillData(server)
      }
    }
  }, [state])

  const create = () => {
    const server: GameServer = { id: uuid, name, cluster, description, ip, portGame, portQuery, portRCON, platforms }
    const nextServers: GameServer[] = [...state.servers, server]
    patchState({ servers: nextServers })
    // setTimeout(() => history.push(`/server/${uuid}`), 100)
    setTimeout(() => history.push('/servers'), 100)
  }

  const update = () => {
    const server: GameServer = { id: uuid, name, cluster, description, ip, portGame, portQuery, portRCON, platforms }
    const nextServers: GameServer[] = [...state.servers.filter(s => s.id !== uuid), server]
    patchState({ servers: nextServers })
    setTimeout(() => history.push('/servers'), 100)
  }

  const remove = () => {
    if (!confirm('confirm delete')) {
      return
    }
    const nextServers: GameServer[] = state.servers.filter(s => s.id !== uuid)
    patchState({ servers: nextServers })
    setTimeout(() => history.push('/servers'), 100)
  }

  const onSubmit = (event: SyntheticEvent) => {
    event.preventDefault()
    console.warn({ event, id, name, description, ip, portGame, portQuery, portRCON, platforms }) // TODO: remove
    if (isCreating) {
      create()
    } else {
      update()
    }
  }

  return (
    <div className="view">
      <div className="self-start w-full p-10">
        <form onSubmit={onSubmit} className="w-full flex flex-col md:flex-row">
          <div className="w-full md:w-1/2">
            {/*<div className="form-control mb-2">*/}
            {/*  <label className="label">*/}
            {/*    <span className="label-text">unique id:</span>*/}
            {/*  </label>*/}
            {/*  <input type="text" className="input input-bordered" value={uuid} onChange={e => setUuid(e.target.value)} />*/}
            {/*</div>*/}
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">name:</span>
              </label>
              <input
                type="text"
                className="input input-bordered"
                value={name}
                onChange={e => setName(e.target.value)}
              />
            </div>
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">description:</span>
              </label>
              <input
                type="text"
                className="input input-bordered"
                value={description}
                onChange={e => setDescription(e.target.value)}
              />
            </div>
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">cluster:</span>
              </label>
              <input
                type="text"
                className="input input-bordered"
                value={cluster}
                onChange={e => setCluster(e.target.value)}
              />
            </div>
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">ip:</span>
              </label>
              <input type="text" className="input input-bordered" value={ip} onChange={e => setIp(e.target.value)} />
            </div>
          </div>
          <div className="w-full md:w-1/2 md:pl-5">
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">game port:</span>
              </label>
              <input
                type="number"
                className="input input-bordered"
                value={`${portGame}`}
                onChange={e => setPortGame(Number(e.target.value))}
              />
            </div>
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">query port:</span>
              </label>
              <input
                type="number"
                className="input input-bordered"
                value={`${portQuery}`}
                onChange={e => setPortQuery(Number(e.target.value))}
              />
            </div>
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">RCON port:</span>
              </label>
              <input
                type="number"
                className="input input-bordered"
                value={`${portRCON}`}
                onChange={e => setPortRCON(Number(e.target.value))}
              />
            </div>
            <div className="form-control mb-2">
              <label className="label">
                <span className="label-text">platforms:</span>
              </label>
              <div className="flex mt-2">
                {allPlatforms.map(p => (
                  <div
                    key={`platform-${p}`}
                    className={`badge badge-lg cursor-pointer mr-2 ${
                      platforms.includes(p) ? 'badge-primary' : 'badge-ghost'
                    }`}
                    onClick={() =>
                      setPlatforms(platforms.includes(p) ? platforms.filter(pl => pl !== p) : [...platforms, p])
                    }
                  >
                    {p}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </form>
        <div className="flex flex-row mt-2 justify-end">
          <div>
            <NavLink to={'/servers'} className="btn">
              back
            </NavLink>
          </div>
          {!isCreating && (
            <button className="btn btn-error ml-2" onClick={remove}>
              delete
            </button>
          )}
          <button className="btn btn-primary ml-2" onClick={onSubmit} disabled={name?.length < 3}>
            {isCreating ? 'create' : 'update'}
          </button>
        </div>
      </div>
    </div>
  )
}
