import React, { useEffect, useState } from 'react'
import { formatCountdown } from '../lib'

type Props = {
  targetTime: number
}
export const Countdown: React.FC<Props> = ({ targetTime }) => {
  const [countdown, setCountdown] = useState<string>('')

  useEffect(() => {
    const i = setInterval(() => setCountdown(formatCountdown(+new Date(targetTime) - +new Date())), 999)
    return () => {
      clearInterval(i)
    }
  }, [])

  return <span>{countdown}</span>
}
