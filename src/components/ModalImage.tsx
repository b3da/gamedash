import React, { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { Image as Img } from './Image'

type Props = {
  isShown?: boolean
  onClose?: Function
}
type State = {
  isOpen: boolean
  image: string
  background: string
}
export class ModalImage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      isOpen: false,
      image: '',
      background: 'white',
    }
  }

  closeModal = () => {
    this.setState({ isOpen: false })
    if (typeof this.props.onClose === 'function') {
      this.props.onClose()
    }
  }

  openModal = (image: string, background: string = 'white') => this.setState({ isOpen: true, image, background })

  render() {
    const { isOpen, image, background } = this.state
    return (
      <div>
        {/*<button className={' btnbtn-primary'} onClick={() => this.openModal('')} >open</button>*/}

        <Transition appear show={isOpen} as={Fragment}>
          <Dialog onClose={() => this.closeModal()} as="div" className="fixed inset-0 z-10 overflow-y-auto">
            <div className="min-h-screen px-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Dialog.Overlay className="fixed inset-0" />
              </Transition.Child>

              {/* centers modal */}
              <span className="inline-block h-screen align-middle" aria-hidden="true">
                &#8203;
              </span>
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <div
                  style={{ backgroundColor: background }}
                  className="inline-block w-full max-w-xl p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl bg-base-blue"
                >
                  <div className="mt-2">
                    <div className="card">
                      <Img src={image} title={''} width={'auto'} onClick={() => this.closeModal()} />
                    </div>
                  </div>

                  {/*<div className="mt-4 flex justify-end gap-2">*/}
                  {/*  <button*/}
                  {/*    onClick={() => this.closeModal()}*/}
                  {/*    className={'btn btn-outline border-light-grey'}>*/}
                  {/*    OK*/}
                  {/*  </button>*/}
                  {/*</div>*/}
                </div>
              </Transition.Child>
            </div>
          </Dialog>
        </Transition>
      </div>
    )
  }
}
