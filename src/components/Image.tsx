import React from 'react'

type Props = {
  src: string
  title: string
  width: string
  padding?: string
  hasError?: boolean
  onClick: Function | any
}
export const Image: React.FC<Props> = ({ src, title, width, padding, hasError, onClick }) => {
  return (
    <div
      className="overflow-hidden flex flex-col items-center justify-center"
      style={{ width, height: width, padding }}
    >
      {/*<img className={'img'} src={src} alt={title} title={title} onClick={onClick} style={{ transform: 'scale(1.5)' }} draggable={false} />*/}
      <img
        className={'img rounded'}
        src={src}
        alt={title}
        title={title}
        onClick={onClick}
        style={{ transform: 'scale(1.02)' }}
        draggable={false}
      />
      {hasError && <div style={{ color: 'red', position: 'absolute', fontSize: '6rem' }}>nope</div>}
    </div>
  )
}
