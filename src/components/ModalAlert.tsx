import React, { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'

type Props = {
  isShown?: boolean
  onClose?: Function
}
type State = {
  isOpen: boolean
  messages: string[]
  background: string
}
export class ModalAlert extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      isOpen: false,
      messages: [],
      background: 'black',
    }
  }

  closeModal = () => {
    this.setState({ isOpen: false })
    if (typeof this.props.onClose === 'function') {
      this.props.onClose()
    }
  }

  openModal = (messages: string[], background: string = 'white') =>
    this.setState({ isOpen: true, messages, background })

  render() {
    const { isOpen, messages, background } = this.state
    return (
      <div>
        {/*<button className={' btnbtn-primary'} onClick={() => this.openModal()} >open</button>*/}

        <Transition appear show={isOpen} as={Fragment}>
          <Dialog onClose={() => this.closeModal()} as="div" className="fixed inset-0 z-10 overflow-y-auto">
            <div className="min-h-screen px-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Dialog.Overlay className="fixed inset-0" />
              </Transition.Child>

              {/* centers modal */}
              <span className="inline-block h-screen align-middle" aria-hidden="true">
                &#8203;
              </span>
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <div
                  style={{ backgroundColor: background }}
                  className="inline-block w-full max-w-xl p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl bg-base-blue"
                >
                  {messages?.length > 0 &&
                    messages.map(message => (
                      <div className="flex justify-between items-center mb-5" key={message}>
                        <Dialog.Title as="h3" className="text-lg font-bold leading-6 text-black">
                          {message}
                        </Dialog.Title>
                      </div>
                    ))}
                  {/*<div className="mt-2">*/}
                  {/*  <div className="card">*/}
                  {/*    {message}*/}
                  {/*  </div>*/}
                  {/*</div>*/}

                  <div className="mt-4 flex justify-end gap-2">
                    <button onClick={() => this.closeModal()} className={'btn btn-outline border-light-grey'}>
                      OK
                    </button>
                  </div>
                </div>
              </Transition.Child>
            </div>
          </Dialog>
        </Transition>
      </div>
    )
  }
}
