import React from 'react'

export const Loader = () => {
  return (
    <div
      className={
        'fixed top-0 left-0 right-0 bottom-0 w-full h-screen z-50 overflow-hidden opacity-75 flex flex-col items-center justify-center'
      }
    >
      <div className={'loader ease-linear'}></div>
    </div>
  )
}
