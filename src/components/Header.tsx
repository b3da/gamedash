import React, { SyntheticEvent, useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { CSSTransition, TransitionGroup } from 'react-transition-group'

export const Header = () => {
  const [currentRoute, setCurrentRoute] = useState<string>('')
  const [searchPhrase, setSearchPhrase] = useState<string>('')
  const history = useHistory()
  const location = useLocation()

  useEffect(() => {
    setCurrentRoute(location?.pathname || '')
  }, [location])

  const search = (event: SyntheticEvent) => {
    event.preventDefault()
    alert('todo server/rule/record switch\n\n' + searchPhrase)
  }

  return (
    <div className="navbar mb-2 shadow-lg bg-neutral text-neutral-content">
      <div className="flex">
        <button
          disabled={currentRoute === '/dashboard'}
          className="btn btn-square btn-ghost"
          onClick={() => (currentRoute !== '/dashboard' ? history.push('/dashboard') : null)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            className="inline-block w-6 h-6 stroke-current"
          >
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
          </svg>
        </button>
      </div>
      <div className="flex flex-1 px-2 mx-2">
        <span className="text-lg font-bold">GameDash</span>
      </div>
      <TransitionGroup>
        <CSSTransition timeout={500} classNames="fade" key={location?.key}>
          <div>
            {(currentRoute === '/servers' || currentRoute === '/rules' || currentRoute === '/records') && (
              <form className="relative" onSubmit={search}>
                <input
                  type="text"
                  className="input input-ghost"
                  placeholder="Search"
                  value={searchPhrase}
                  onChange={e => setSearchPhrase(e.target.value)}
                />
                <button
                  onClick={search}
                  className="absolute top-0 right-0 rounded-l-none btn btn-ghost"
                  style={{ borderTopLeftRadius: '0px', borderBottomLeftRadius: '0px' }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    className="inline-block w-4 h-4 stroke-current"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    />
                  </svg>
                </button>
              </form>
            )}
          </div>
        </CSSTransition>
      </TransitionGroup>

      {/*<div className="flex-1 lg:flex-none">*/}
      {/*  <div className="form-control">*/}
      {/*    <input*/}
      {/*      type="text"*/}
      {/*      className="input input-ghost"*/}
      {/*      placeholder="Search"*/}
      {/*      value={searchPhrase}*/}
      {/*      onChange={e => setSearchPhrase(e.target.value)} onSubmit={search} />*/}
      {/*  </div>*/}
      {/*</div>*/}
      {/*<div className="flex-none">*/}
      {/*  <button className="btn btn-square btn-ghost">*/}
      {/*    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"*/}
      {/*         className="inline-block w-6 h-6 stroke-current">*/}
      {/*      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"*/}
      {/*            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>*/}
      {/*    </svg>*/}
      {/*  </button>*/}
      {/*</div>*/}
      {/*<div className="flex-none">*/}
      {/*  <button className="btn btn-square btn-ghost">*/}
      {/*    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"*/}
      {/*         className="inline-block w-6 h-6 stroke-current">*/}
      {/*      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"*/}
      {/*            d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"></path>*/}
      {/*    </svg>*/}
      {/*  </button>*/}
      {/*</div>*/}
      {/*<div className="flex-none">*/}
      {/*  <div className="avatar">*/}
      {/*    <div className="rounded-full w-10 h-10 m-1">*/}
      {/*      <img src="https://i.pravatar.cc/500?img=32"></img>*/}
      {/*    </div>*/}
      {/*  </div>*/}
      {/*</div>*/}
    </div>
  )
}
