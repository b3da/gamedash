import { StoreState } from './StoreContext'

export const generateUuidV4 = () =>
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    const r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8
    return v.toString(16)
  })

export const getRndFromArray = (arr: any[], count: number = 1): any[] | any => {
  if (!arr || !arr?.length) {
    return count > 1 ? [] : undefined
  }
  // if (count === 1) {
  //   return arr[Math.floor(Math.random() * arr.length)]
  // }
  const res: any[] = []
  const used: number[] = []
  while (res.length < count && arr.length > used.length) {
    const x = Math.floor(Math.random() * arr.length)
    if (!used.includes(x)) {
      res.push(arr[x])
      used.push(x)
    }
  }
  const uniq = Array.from(new Set(res))
  return uniq?.length === 1 ? uniq[0] : uniq
}

export const shuffleArray = (arr: any[]) =>
  arr
    .map((value: any) => ({ value, sort: Math.random() }))
    .sort((a: any, b: any) => a.sort - b.sort)
    .map(({ value }: any) => value)
export type GameStatus = 'success' | 'error' | null

export const vibrate = (pattern: number | number[] = 100) => {
  if (window.navigator.vibrate) {
    window.navigator.vibrate(pattern)
  }
}

export const extractDataForUser = (user: string, state: StoreState) => {
  if (state.records.filter(r => r.data.user === user).length === 0) {
    return null
  }
  const MONTH_MS = 1000 * 60 * 60 * 24 * 30.5
  const donations = state.records
    .filter(r => r.data.user === user && r.set === 'donator' && r.data.balance >= 500)
    .sort((a, b) => (a.data.createdOn === b.data.createdOn ? 0 : a.data.createdOn < b.data.createdOn ? 1 : -1))
  const lastDonationDate = donations.length > 0 ? donations[0].data.createdOn : 0
  const lastDonationAmount = donations.length > 0 ? donations[0].data.balance : 0
  const lastDonationDurationMs = Math.floor(lastDonationAmount / 100) * MONTH_MS
  const hasActiveMembership = lastDonationDate + lastDonationDurationMs > +new Date()
  let balanceEventPoints = 0
  try {
    balanceEventPoints = state.records
      .filter(r => r.data.user === user && (r.set === 'event' || r.set === 'arena' || r.set === 'withdrawal'))
      .map(r => r.data.balance)
      .reduce((a, b) => Number(a) + Number(b))
  } catch (_) {}
  let donatedSum = 0
  try {
    donatedSum = state.records
      .filter(r => r.data.user === user && r.set === 'donator')
      .map(r => r.data.balance)
      .reduce((a, b) => Number(a) + Number(b))
  } catch (_) {}
  const recordsInSets: any[] = []
  if (state.records.filter(r => r.data.user === user && r.set === 'arena').length > 0) {
    recordsInSets.push({
      set: 'arena',
      data: state.records.filter(r => r.data.user === user && r.set === 'arena').map(r => r.data),
    })
  }
  if (state.records.filter(r => r.data.user === user && r.set === 'donator').length > 0) {
    recordsInSets.push({
      set: 'donator',
      data: state.records.filter(r => r.data.user === user && r.set === 'donator').map(r => r.data),
    })
  }
  if (state.records.filter(r => r.data.user === user && r.set === 'event').length > 0) {
    recordsInSets.push({
      set: 'event',
      data: state.records.filter(r => r.data.user === user && r.set === 'event').map(r => r.data),
    })
  }
  return {
    user,
    balanceEventPoints,
    isDonator: state.records.filter(r => r.data.user === user && r.set === 'donator').length > 0,
    donatedSum: donatedSum || undefined,
    hasActiveMembership,
    membershipEnd:
      lastDonationDurationMs > 0 ? new Date(lastDonationDate + lastDonationDurationMs).toISOString() : undefined,
    membershipDays:
      lastDonationDurationMs > 0
        ? ((+new Date(lastDonationDate + lastDonationDurationMs) - +new Date()) / 1000 / 60 / 60 / 24).toFixed(4)
        : undefined,
    records: recordsInSets,
  }
}

export const extractDataForAllUsers = (state: StoreState) => {
  const users = Array.from(new Set(state.records.map(r => r.data.user)))
  const res = []
  for (const u of users) {
    res.push(extractDataForUser(u, state))
  }
  return res
}

export const formatCountdown = (ms: number) => {
  const days = Math.floor(ms / (24 * 60 * 60 * 1000))
  const daysMs = ms % (24 * 60 * 60 * 1000)
  const hours = Math.floor(daysMs / (60 * 60 * 1000))
  const hoursMs = ms % (60 * 60 * 1000)
  const minutes = Math.floor(hoursMs / (60 * 1000))
  const minutesMs = ms % (60 * 1000)
  const sec = Math.floor(minutesMs / 1000)
  return days + ':' + hours + ':' + minutes + ':' + sec
}
