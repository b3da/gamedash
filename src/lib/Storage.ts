export class Storage {
  static STORAGE_KEY = 'gamedash'

  static set(value: any): void {
    window.localStorage.setItem(Storage.STORAGE_KEY, JSON.stringify(value))
  }

  static get(): any {
    const str: string | null = window.localStorage.getItem(Storage.STORAGE_KEY)
    if (!str) {
      return null
    }
    try {
      return JSON.parse(str)
    } catch (e) {
      console.warn(e)
    }
  }

  static remove(): void {
    window.localStorage.removeItem(Storage.STORAGE_KEY)
  }

  static clear(): void {
    window.localStorage.clear()
  }
}
