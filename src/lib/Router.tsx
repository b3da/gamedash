import React, { useEffect, useState } from 'react'
import { BrowserRouter as RouterModule, Switch, Route, Redirect, useLocation } from 'react-router-dom'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { useStore } from './'
import {
  Dashboard,
  JSONViewer,
  Login,
  NotFound404,
  PublicHome,
  Record,
  Records,
  Rule,
  Rules,
  Server,
  Servers,
  User,
} from '../views'
import { Header, Loader } from '../components'

type Props = {}
export const Router: React.FC<Props> = () => {
  const [isReady, setIsReady] = useState(false)

  useEffect(() => {
    setIsReady(true)
  }, [])
  if (!isReady) {
    return <Loader />
  }
  return (
    <RouterModule>
      <Header />
      <Routes />
    </RouterModule>
  )
}

const Routes = () => {
  const location: any = useLocation()
  return (
    <main id={'router'}>
      {/*<RootRedirect />*/}
      <TransitionGroup>
        <CSSTransition timeout={500} classNames="reroute" key={location?.key}>
          <Switch location={location}>
            <Route exact path="/">
              <PublicHome />
            </Route>
            <Route path="/login">
              <Login />
            </Route>
            <AuthenticatedRoute path="/dashboard">
              <Dashboard />
            </AuthenticatedRoute>
            <AuthenticatedRoute path="/servers">
              <Servers />
            </AuthenticatedRoute>
            <AuthenticatedRoute path="/server/:id">
              <Server />
            </AuthenticatedRoute>
            <AuthenticatedRoute path="/server">
              <Server />
            </AuthenticatedRoute>
            <AuthenticatedRoute path="/rules">
              <Rules />
            </AuthenticatedRoute>
            <AuthenticatedRoute path="/rule/:id">
              <Rule />
            </AuthenticatedRoute>
            <AuthenticatedRoute path="/rule">
              <Rule />
            </AuthenticatedRoute>
            <AuthenticatedRoute path="/records">
              <Records />
            </AuthenticatedRoute>
            <AuthenticatedRoute path="/record/:id/:username">
              <Record />
            </AuthenticatedRoute>
            <AuthenticatedRoute path="/record/:id">
              <Record />
            </AuthenticatedRoute>
            <AuthenticatedRoute path="/record">
              <Record />
            </AuthenticatedRoute>
            <AuthenticatedRoute path="/user/:username">
              <User />
            </AuthenticatedRoute>

            <AuthenticatedRoute path="/json/:resource">
              <JSONViewer />
            </AuthenticatedRoute>
            <Route path="*">
              <NotFound404 />
            </Route>
          </Switch>
        </CSSTransition>
      </TransitionGroup>
    </main>
  )
}

const AuthenticatedRoute: React.FC<any> = ({ children, ...props }) => {
  const { state } = useStore()
  return (
    <Route
      {...props}
      render={({ location }) =>
        state?.username?.length > 0 ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  )
}

const RootRedirect = () => {
  const location: any = useLocation()
  if (location.pathname === '/') {
    return (
      <Redirect
        to={{
          pathname: '/login',
          state: { from: location },
        }}
      />
    )
  }
  return null
}
