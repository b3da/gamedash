import React, { Component, createContext, useContext, useState } from 'react'
import { Storage } from './'

export type GameServer = {
  id: string
  name: string
  cluster: string
  description: string
  ip: string
  portGame: number
  portQuery: number
  portRCON: number
  platforms: GamingPlatform[]
}
export type GamingPlatform = 'steam' | 'epic' | 'xbox' | 'ps'
export type RecordSet = 'arena' | 'donator' | 'event' | 'withdrawal' // todo make editable?
export type Record = {
  id: number
  set: RecordSet
  data: {
    balance: number
    createdBy: string
    createdOn: number
    note: string
    user: string
  }
}
export type Rule = {
  cluster: string
  rule: string
}
export type Rules = {
  [id: number]: Rule
}
export type StoreState = {
  username: string
  records: Record[]
  rules: Rules
  servers: GameServer[]
}
export type StoreContextType = {
  state: Readonly<StoreState>
  setState: (nextState: StoreState) => StoreState | any
  patchState: (nextStatePart: Partial<StoreState>) => StoreState | any
}
export const StoreContext: React.Context<StoreContextType> = createContext<StoreContextType>({
  state: {
    username: '',
    records: [],
    rules: {},
    servers: [],
  },
  setState: () => {},
  patchState: () => {},
})

type Props = {
  children: Component | Element | any
}

export const StoreContextProvider: React.FC<Props> = ({ children }) => {
  const ctx = createStoreContext()
  return <StoreContext.Provider value={{ ...ctx }}>{children}</StoreContext.Provider>
}

export const useStore = (): StoreContextType => {
  return useContext(StoreContext)
}

const createStoreContext = (): StoreContextType => {
  const initState = Storage.get() || {
    username: '',
    records: [],
    rules: {},
    servers: [],
  }
  const [state, setState] = useState<StoreState>(initState)
  const replaceState = (nextState: StoreState) => {
    Storage.set(nextState)
    setState(nextState)
  }
  const patchState = (nextStatePart: Partial<StoreState>) => {
    // console.warn('patchState', {...nextStatePart}, {...state}) // TODO: remove
    const nextState = { ...state, ...nextStatePart }
    Storage.set(nextState)
    setState(nextState)
  }
  return {
    state,
    setState: replaceState,
    patchState,
  }
}
