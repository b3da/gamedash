#!/usr/bin/env bash

yarn build

cp -f .htaccess ./build
#cp -f .htpasswd ./build

rsync -avx --delete-before  ./build/* b3da@u18041.ryzen.server:/var/www/gamedash.rnd.blue/web
rsync -avx --delete-before  ./build/.htaccess b3da@u18041.ryzen.server:/var/www/gamedash.rnd.blue/web
#rsync -avx --delete-before  ./build/.htpasswd b3da@u18041.ryzen.server:/var/www/gamedash.rnd.blue/web

echo "done"
