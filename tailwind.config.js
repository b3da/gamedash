const defaultTheme = require('tailwindcss/defaultTheme')
const plugin = require('tailwindcss/plugin')

module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './src/components/**/*.{js,jsx,ts,tsx}', './index.tsx', './public/index.html'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        'primary-blue': '#0e77a7',
        'secondary-blue': '#F2F7FF',
        'base-blue': '#F4F7FC',
        'sky-blue': '#57B8FF',
        'sky-light': '#EEF8FF',
        'cloud-blue': '#E5EFFE',
        'light-blue': '#A8C6FD',
        indigo: '#EBEDFF',
        'indigo-dark': '#6979F8',
        violet: '#F3EDFF',
        purple: '#8950FC',
        'primary-grey': '#F4F7FC',
        'secondary-grey': '#F3F3F4',
        'light-grey': '#D7DBEC',
        'dark-grey': '#5A607F',
        'cool-grey': '#E6E9F4',
        'shadow-grey': '#7E84A3',
        orange: '#F99600',
        'orange-light': '#FFF4E5',
        green: '#21D59B',
        'green-light': '#E8FBF5',
        danger: '#F0142F',
      },
    },
    screens: {
      xs: { max: '820px' },
      ...defaultTheme.screens,
    },
  },
  variants: {
    extend: {
      ringWidth: ['hover', 'active'],
    },
  },
  plugins: [require('daisyui')],
  daisyui: {
    styled: true,
    base: true,
    utils: true,
    logs: true,
    rtl: false,
    themes: [
      {
        mytheme: {
          primary: '#0e77a7',
          'primary-focus': '#0e77a7',
          'primary-content': '#ffffff',
          secondary: '#a6b9b7',
          'secondary-focus': '#a6b9b7',
          'secondary-content': '#a6b9b7',
          accent: '#37cdbe',
          'accent-focus': '#2aa79b',
          'accent-content': '#ffffff',
          neutral: '#3d4451',
          'neutral-focus': '#2a2e37',
          'neutral-content': '#ffffff',
          'base-100': '#000',
          'base-200': '#2a2e37',
          'base-300': '#333742',
          'base-content': '#fff',
          info: '#c1deff',
          success: '#009485',
          warning: '#ff9900',
          error: '#ff5724',
        },
      },
      'dark',
    ],
  },
}
